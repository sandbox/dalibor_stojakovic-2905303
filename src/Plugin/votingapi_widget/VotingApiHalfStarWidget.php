<?php

namespace Drupal\votingapi_halfstar\Plugin\votingapi_widget;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\votingapi_widgets\Plugin\VotingApiWidgetBase;

/**
 * Assigns ownership of a node to a user.
 *
 * @VotingApiWidget(
 *   id = "halfstar",
 *   label = @Translation("Halfstar rating")
 * )
 */
class VotingApiHalfStarWidget extends VotingApiWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function getInitialVotingElement(array &$form) {
    $form['value']['#prefix'] = '<div class="votingapi-halfstar halfstar">';
    $form['value']['#attached'] = [
      'library' => ['votingapi_halfstar/votingapi-halfstar'],
    ];
    $form['value']['#suffix'] = '</div>';
    $form['value']['#attributes'] = [
      'data-style' => 'default',
      'data-is-edit' => 1,
    ];
  }

  /**
   * Get Values.
   *
   * @return array
   *    Posible values
   */
  public function getValues() {
    return [
      '0.5' => 0.5,
      '1' => 1,
      '1.5' => 1.5,
      '2' => 2,
      '2.5' => 2.5,
      '3' => 3,
      '3.5' => 3.5,
      '4' => 4,
      '4.5' => 4.5,
      '5' => 5,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getForm($entity_type, $entity_bundle, $entity_id, $vote_type, $field_name, $style, $show_results, $read_only, $show_own_vote) {
    $vote = $this->getEntityForVoting($entity_type, $entity_bundle, $entity_id, $vote_type, $field_name);
    /*
     * @TODO: remove custom entity_form_builder once
     *   https://www.drupal.org/node/766146 is fixed.
     */
    return \Drupal::service('entity.form_builder')->getForm($vote, 'votingapi_' . $this->getPluginId(), [
      'read_only' => $read_only,
      'options' => $this->getValues(),
      'style' => $style,
      'show_results' => $show_results,
      'show_own_vote' => $show_own_vote,
      'plugin' => $this,
      // @TODO: following keys can be removed once #766146 is fixed.
      'entity_type' => $entity_type,
      'entity_bundle' => $entity_bundle,
      'entity_id' => $entity_id,
      'vote_type' => $vote_type,
      'field_name' => $field_name,
    ]);
  }

  /**
   * Vote form.
   */
  public function buildForm($entity_type, $entity_bundle, $entity_id, $vote_type, $field_name, $style, $show_results, $read_only = FALSE, $show_own_vote = FALSE) {
    $form = $this->getForm($entity_type, $entity_bundle, $entity_id, $vote_type, $field_name, $style, $show_results, $read_only, $show_own_vote);
    $build = [
      'rating' => [
        '#theme' => 'container',
        '#attributes' => [
          'class' => [
            'votingapi-widgets',
            'halfstar',
            ($read_only) ? 'read_only' : '',
          ],
        ],
        '#children' => [
          'form' => $form,
        ],
      ],
      '#attached' => [
        'library' => [
          'votingapi_halfstar/votingapi-halfstar',
        ],
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyles() {
    return [
      'default' => t('Default'),
    ];
  }

  /**
   * Generate summary.
   */
  public function getVoteSummary(ContentEntityInterface $vote) {
    $results = $this->getResults($vote);
    $field_name = $vote->field_name->value;
    $fieldResults = [];

    foreach ($results as $key => $result) {
      if (strrpos($key, $field_name) !== FALSE) {
        $key = explode(':', $key);
        $fieldResults[$key[0]] = ($result != 0) ? $result * 10 / 10 : 0;
      }
    }

    return [
      '#theme' => 'votingapi_halfstar_summary',
      '#vote' => $vote,
      '#results' => $fieldResults,
      '#field_name' => $vote->field_name->value,
    ];
  }

}
