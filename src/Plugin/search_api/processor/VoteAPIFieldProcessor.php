<?php

namespace Drupal\votingapi_halfstar\Plugin\search_api\processor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SearchApiProcessor.
 *
 * @SearchApiProcessor(
 *   id = "votingapi_halfstar",
 *   label = @Translation("Halfstar Processor"),
 *   description = @Translation("Enabling indexing for average rating in Search API index"),
 *   stages = {
 *     "pre_index_save" = -10,
 *     "preprocess_index" = -30
 *   },
 * )
 */
class VoteAPIFieldProcessor extends ProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Result Function.
   *
   * @var \Drupal\votingapi\VoteResultFunctionManager
   */
  protected $resultFunction;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, $resultFunction) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->resultFunction = $resultFunction;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.votingapi.resultfunction')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {
    foreach ($items as $item) {
      /* @var \Drupal\node\Entity\Node $entity */
      $entity = $item->getOriginalObject()->getValue();
      $fields = $item->getFields();
      /* @var \Drupal\search_api\Item\Field $field */
      foreach ($fields as $field) {
        if ($field->getDataDefinition()->getDataType() == 'field_item:voting_api_field') {
          $results = $this->resultFunction->getResults($entity->getEntityTypeId(), $entity->id());
          if ($results) {
            $field->addValue($results['vote']['vote_average']);
          }
        }
      }
    }
  }

}
