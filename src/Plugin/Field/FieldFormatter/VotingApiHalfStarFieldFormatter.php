<?php

namespace Drupal\votingapi_halfstar\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'half_star_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "half_star_field_formatter",
 *   label = @Translation("Half star field formatter"),
 *   field_types = {
 *     "voting_api_field"
 *   }
 * )
 */
class VotingApiHalfStarFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Result Function.
   *
   * @var \Drupal\votingapi\VoteResultFunctionManager
   */
  protected $resultFunction;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, $resultFunction) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->resultFunction = $resultFunction;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.votingapi.resultfunction')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rating_average_min' => 0,
      'rating_count_min' => 1,
      'show_count' => 0,
      'stars_style' => 'single_star',
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
      'rating_average_min' => [
        '#type' => 'number',
        '#title' => t("Don't show if rating average is below"),
        '#description' => t('Set to zero to always display'),
        '#default_value' => $this->getSetting('rating_average_min'),
        '#min' => 0,
      ],
      'rating_count_min' => [
        '#type' => 'number',
        '#title' => t('Minimum number of ratings before showing average'),
        '#description' => t('Set to 1 for display when there is any vote available'),
        '#default_value' => $this->getSetting('rating_count_min'),
        '#min' => 1,
      ],
      'show_count' => [
        '#title' => $this->t('Show count'),
        '#type'          => 'checkbox',
        '#default_value' => $this->getSetting('show_count'),
      ],
      'stars_style' => [
        '#title' => $this->t('Style'),
        '#type' => 'select',
        '#options' => [
          'single_star' => 'Single star',
          'fa_stars' => 'Fa stars',
          'rateyo' => 'RateYo',
        ],
        '#default_value' => $this->getSetting('stars_style'),
      ],

    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    $summary[] = t('Rating average min before showing: @average', array('@average' => $this->getSetting('rating_average_min')));
    $summary[] = t('Rating count min before showing: @count', array('@count' => $this->getSetting('rating_count_min')));
    $summary[] = t('Show count: @count', array('@count' => $this->getSetting('show_count')));
    $summary[] = t('Style: @style', array('@style' => $this->getSetting('stars_style')));
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $config = $this->getSettings();
    if ($entity = $items->getEntity()) {
      $results = $this->resultFunction->getResults($entity->getEntityTypeId(), $entity->id());
      if ($results) {
        foreach ($results as $result) {
          $field_name = $items->getFieldDefinition()->getName();
          $average_field_name = 'vote_field_average_halfstar:' . $entity->getEntityTypeId() . '.' . $field_name;
          if (isset($result[$average_field_name]) && $result['vote_count'] >= $config['rating_count_min'] && $result[$average_field_name] >= $config['rating_average_min']) {
            switch ($config['stars_style']) {
              case 'single_star':
                $elements[0]['#markup'] = '<span class="fa-stack fa-1x"><i class="fa fa-star fa-stack-2x"></i><strong class="fa-stack-1x">'
                    . round($result[$average_field_name], 1, PHP_ROUND_HALF_DOWN);
                if ($config['show_count']) {
                  $elements[0]['#markup'] .= "(" . $result['vote_count'] . ")";
                }
                $elements[0]['#markup'] .= '</strong></span>';
                break;

              case 'fa_stars':
                $average = round($result[$average_field_name] * 2) / 2;
                $elements[0]['#markup'] = '';
                $elements[0]['#markup'] .= round($result[$average_field_name], 1, PHP_ROUND_HALF_DOWN) . ' ';
                while ($average >= 1) {
                  $elements[0]['#markup'] .= '<i class="fa fa-star" aria-hidden="true"></i>';
                  $average--;
                }
                if ($average >= 0.5) {
                  $elements[0]['#markup'] .= '<i class="fa fa-star-half" aria-hidden="true"></i>';
                }
                $elements[0]['#markup'] .= ' (' . $result['vote_count'] . ')';
                break;

              case 'rateyo':
                $elements[0]['#markup'] = '<div class="rateyo-readonly"><span class="rateyo-average">'
                    . round($result[$average_field_name], 1, PHP_ROUND_HALF_DOWN) . '</span><span class="rateyo-widget"></span>';
                if ($config['show_count']) {
                  $elements[0]['#markup'] .= "(" . '<span class="rateyo-count">' . $result['vote_count'] . '</span>' . ")";
                }
                $elements[0]['#markup'] .= '</div>';
                break;

            }
            $elements[0]['#attached']['library'][] = 'votingapi_halfstar/votingapi-halfstar';

          }
        }
      }
    }
    return $elements;
  }

}
