<?php

namespace Drupal\votingapi_halfstar\Plugin\VoteResultFunction;

use Drupal\votingapi_widgets\FieldVoteResultBase;

/**
 * A sum of a set of votes.
 *
 * @VoteResultFunction(
 *   id = "vote_field_average_halfstar",
 *   label = @Translation("Average"),
 *   description = @Translation("The average vote value."),
 *   deriver = "Drupal\votingapi_widgets\Plugin\Derivative\FieldResultFunction",
 * )
 */
class FieldAverage extends FieldVoteResultBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes) {
    $total = 0;
    $votes = $this->getVotesForField($votes);
    foreach ($votes as $vote) {
      // Changed int to float.
      $total += (float) $vote->getValue();
    }
    if ($total == 0) {
      return 0;
    }
    return ($total / count($votes));
  }

}
