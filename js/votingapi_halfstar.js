(function ($) {
  Drupal.behaviors.votingapiHalfstar = {
    attach: function (context, settings) {
      $(document).ready(function() {
        var rateyo = $('.rateyo');
        $('.vote-form .form-submit').hide();
        rateyo.once('rateyosetup').on('rateyo.set', function (e, data) {
          $(this).closest('form').find('select').val(data.rating);
          $(this).closest('form').find('.form-submit').click();
        });
        $('.vote-form select[name="value"]').parent().hide();
        rateyo.once('rateyoinit').each(function() {
          var vote_average_field = $(this).closest('form').find('.vote-average');
          var vote_average = (vote_average_field.length) ? (Math.floor(vote_average_field.html() * 2) / 2).toFixed(1) : false;
          if(vote_average){
            $(this).rateYo({
              starWidth: '35px',
              halfStar: true,
              rating: vote_average
            });
          }else{
            $(this).rateYo({
              starWidth: '35px',
              halfStar: true,
            });
          }
        });
      });
    }
  };
}(jQuery));
